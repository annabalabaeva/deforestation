"""
Wrapper for hdf5 database writer.
"""

import h5py
import numpy as np


class H5Wrapper:

    def __init__(self, name, lim):
        '''
        Creates hdf5 database.
        :param name: name of file,
        lim - limit of cache.
        '''
        assert(lim >= 0)
        self.lim = lim
        self._file = h5py.File(name, "w")
        self._data = {}

    def _push(self, key, chunk):
        # push data into file
        if key not in self._file.keys():
            # create dataset
            self._file.create_dataset(key, tuple([0] + list(chunk[0].shape)),
                                      maxshape=tuple(
                                          [None] + list(chunk[0].shape)),
                                      dtype=chunk[0].dtype, chunks=True)
        if len(chunk) > 0:
            # extend dataset
            self._file[key].resize(self._file[key].shape[
                                   0] + len(chunk), axis=0)
            # write into dataset
            self._file[key][-len(chunk):] = chunk

    def append(self, data):
        """
        Appends data.
        data - dictionary of lists with np arrays to append.
        """
        # appends data
        for key in data:
            if key not in self._data:
                self._data[key] = data[key]
            else:
                self._data[key] += data[key]
        # push data
        data_len = len(list(self._data.values())[0])
        if data_len > self.lim:
            # shuffle chunk
            # assert the same size
            for key in self._data:
                assert data_len == len(self._data[key])
            indices = list(range(data_len))
            np.random.shuffle(indices)
            for key in data:
                tmp = [self._data[key][i] for i in indices]
                self._push(key, tmp)
                # empty cache
                self._data[key] = []
                tmp = []

    def __del__(self):
        """
        Closes database.
        """
        # push the rest data
        if len(self._data.values()) != 0:
            data_len = len(list(self._data.values())[0])
            if data_len > 0:
                # shuffle chunk
                # assert the same size
                indices = list(range(data_len))
                np.random.shuffle(indices)
                for key in self._data:
                    tmp = [self._data[key][i] for i in indices]
                    self._push(key, tmp)
                    # empty cache
                    self._data[key] = []
                    tmp = []
        self._file.close()
