import os

import numpy as np
import cv2
import pandas as pd
from itertools import chain

from tqdm import tqdm

from h5wrapper import H5Wrapper

NEW_SIZE = (224, 224)
CACHE_LIM = 200


class AmazonConverter:

    def __init__(self, data, save, csv):
        self.data_path = data
        self.save_path = save
        self.db = H5Wrapper(save, CACHE_LIM)
        self.labels_frame = pd.read_csv(csv)

    def run(self):
        labels = sorted(set(chain.from_iterable(
            [tags.split(" ") for tags in self.labels_frame['tags'].values])))
        labels_map = {l: i for i, l in enumerate(labels)}
        for i, filename in tqdm(enumerate(os.listdir(self.data_path))):
            img = cv2.imread(
                os.path.join(self.data_path, filename))
            img = cv2.resize(img, NEW_SIZE, interpolation=cv2.INTER_LINEAR)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = img.astype(np.uint8)
            lbls = np.zeros(len(labels_map))
            for t in self.labels_frame.ix[i, 1:].as_matrix().astype('str')[0].split(' '):
                lbls[labels_map[t]] = 1
            lbls = lbls.astype(np.uint8)

            self._add_to_database(img, lbls)

    def _add_to_database(self, img, lbls):
        data = {"imgs": [img],
                "lbls": [lbls]}

        self.db.append(data)


def parse_args():
    """ Parses arguments: creates parser, add command line arguments and calls parser.parse_args.

    Returns:
    Populated namespace (which parser.parse_args returns).
    """
    import argparse
    parser = argparse.ArgumentParser(description='Carvana creation')
    parser.add_argument('--data', type=str,
                        help='Path to direcory with data.')
    parser.add_argument('--csv', type=str,
                        help='Path to direcory with masks.')
    parser.add_argument('--save', type=str,
                        help='Path to file where to save database (.hdf5).')
    args = parser.parse_args()
    return args


def main():
    """ Application entry point.
    """
    args = parse_args()
    assert(args.data is not None)
    assert(args.save is not None)
    converter = AmazonConverter(args.data, args.save, args.csv)
    converter.run()

if __name__ == '__main__':
    main()
