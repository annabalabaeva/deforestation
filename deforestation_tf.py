from __future__ import print_function
from __future__ import division
from __future__ import generators

import tensorflow as tf

from sklearn.model_selection import train_test_split
from sklearn.metrics import fbeta_score

import numpy as np

import argparse
from itertools import chain
import scipy.misc
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import cpu_count
import pandas as pd
import os

import h5py

IMG_KEY = 'img'
LBL_KEY = 'lbl'

INPUT_SIZE = (224, 224)
CLASS_COUNT = 17


class DatasetHelper:

    def __init__(self, data_path, hdf5=False, csv_path=None):
        super(DatasetHelper, self).__init__()
        if hdf5:
            self.db = h5py.File(data_path, 'r')
        else:
            self.root = data_path
            print(data_path)
            self.db = os.listdir(data_path)
            self.csv_path = csv_path

    def len():
        if self.csv_path:
            return len(self.db)
        else:
            return self.db[IMG_KEY].shape[0]

    def _sample_generator_hdf5(self):
        db_len = self.db[IMG_KEY].shape[0]
        print(db_len)
        for idx in np.arange(db_len):
            img = (self.db[IMG_KEY][idx] / 255.0).astype(np.float32)
            lbls = self.db[LBL_KEY][idx]
            yield img, lbls

    def _sample_generator(self):
        labels_frame = pd.read_csv(self.csv_path)
        labels = sorted(set(chain.from_iterable(
            [tags.split(" ") for tags in labels_frame['tags'].values])))
        labels_map = {l: i for i, l in enumerate(labels)}
        print(len(self.db))
        for idx in np.array(len(self.db)):
            print("in_generator")
            lbls = np.zeros(len(labels_map))
            cur_labels = labels_frame.ix[idx, 1:].as_matrix().astype('str')[0]
            for t in cur_labels.split(' '):
                lbls[labels_map[t]] = 1
            img_name = join(self.root, labels_frame.ix[idx, 0] + '.jpg')
            img = scipy.misc.imread(img_name, mode='RGB')
            img = (img / 255.0).astype(np.float32)
            yield img, lbls

    def get_iterator(self, augmentation=False, batch_size=4,
                     num_classes=CLASS_COUNT):
        print("get_iterator")
        if self.csv_path:
            cur_generator = self._sample_generator
        else:
            cur_generator = self._sample_generator_hdf5
        dataset = tf.data.Dataset.from_generator(
            generator=cur_generator,
            output_types=(tf.float32, tf.int32),
            output_shapes=(tf.TensorShape([INPUT_SIZE[0], INPUT_SIZE[1], 3]),
                           tf.TensorShape([num_classes]))
        )
        if augmentation:
            dataset = dataset.map(
                _augmentation, num_parallel_calls=1).repeat()
        dataset = dataset.shuffle(batch_size * 10)
        dataset = dataset.batch(batch_size)
        dataset = dataset.prefetch(batch_size * 10)
        iterator = tf.data.Iterator.from_structure(
            dataset.output_types, dataset.output_shapes)
        initializer = iterator.make_initializer(dataset)
        print("here")
        return initializer, iterator

    def _augmentation(self, img, lbls):
        pass


def define_net(load_path, batch_size, iters_per_epoch):
    #  importing resnet-18
    next_image_placeholder = tf.placeholder(
        tf.float32, (batch_size, INPUT_SIZE[0], INPUT_SIZE[1], 3)
    )
    next_labels_placeholder = tf.placeholder_with_default(
        tf.int8, (batch_size, CLASS_COUNT))
    global_step = tf.Variable(0, trainable=False)
    decayed_lr = tf.train.exponential_decay(
        learning_rate=1e-3,
        global_step=global_step,
        decay_steps=10 * iters_per_epoch,
        decay_rate=0.1,
        staircase=True
    )
    old_saver = tf.train.import_meta_graph(
        load_path,
        input_map={"Placeholder:0": next_image_placeholder,
                   "Placeholder_1:0": next_labels_placeholder,
                   "lr:0": decayed_lr})

    graph = tf.get_default_graph()
    '''
    sess = tf.Session(
        config=tf.ConfigProto(gpu_options={"allow_growth": True}))
    '''
    sess = tf.Session()
    old_saver.restore(sess, load_path)

    old_summaries = tf.get_collection_ref(tf.GraphKeys.SUMMARIES)
    old_summaries.pop()
    old_summaries.pop()

    #  getting nodes from imported graph
    train_placeholder = graph.get_tensor_by_name("is_train:0")
    old_logits = graph.get_tensor_by_name("tower/logits/fc/BiasAdd:0")

    #  adding new outputs
    with tf.name_scope("tower/logits_plants"):
        logits_plants = tf.layers.dense(tf.nn.relu(old_logits),
                                        NUM_CATEGORIES,
                                        name="logits_plants")


def main():
    args = parse_args()

    #img_size = args.img_size
    #num_classes = args.num_classes

    train_dataset_helper = DatasetHelper(args.train_data,
                                         csv_path=args.train_csv)
    valid_dataset_helper = DatasetHelper(args.val_data,
                                         csv_path=args.val_csv)
    train_initializer, train_iterator = train_dataset_helper.get_iterator()
    valid_initializer, valid_iterator = valid_dataset_helper.get_iterator()

    next_image_placeholder = tf.placeholder(
        tf.float32, (4, INPUT_SIZE[0], INPUT_SIZE[1], 3)
    )
    next_labels_placeholder = tf.placeholder(
        tf.int8, (4, CLASS_COUNT))
    # define_net(args.load_model, 4, train_dataset_helper.len())
    with tf.Session() as sess:

        sess.run(train_initializer)
        next_image_placeholder, next_labels_placeholder = train_iterator.get_next()
        print(next_image_placeholder)
        print(next_labels_placeholder)


def parse_args():
    import argparse
    parser = argparse.ArgumentParser(description='Deforestation args parser')
    parser.add_argument('--train', action='store_true',
                        help='Launch in train mode')
    parser.add_argument('--train_csv', type=str,
                        help='Path to file with train images labels')
    parser.add_argument('--train_data', type=str,
                        help='Path to directory with train images')
    parser.add_argument('--val_csv', type=str,
                        help='Path to file with validation images labels')
    parser.add_argument('--val_data', type=str,
                        help='Path to directory with validation images')
    parser.add_argument('--save_model', type=str,
                        help='Path where to save the model')
    parser.add_argument('--load_model', type=str,
                        help='Path where model was saved')
    parser.add_argument('--host', type=str, default='http://localhost',
                        help='Visdom host (localhost by default)')
    parser.add_argument('--port', type=int, default=8097,
                        help='Visdom port (8097 by default)')
    parser.add_argument('--ui', action='store_true', default=False,
                        help='Enable Visdom UI')
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()


'''
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding='SAME')


from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
x = tf.placeholder(tf.float32, shape=[None, 784])
y_ = tf.placeholder(tf.float32, shape=[None, 10])

W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])

x_image = tf.reshape(x, [-1, 28, 28, 1])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])

h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])

y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(20000):
        batch = mnist.train.next_batch(50)
        if i % 500 == 0:
            train_accuracy = accuracy.eval(feed_dict={
                x: batch[0], y_: batch[1], keep_prob: 1.0})
            print('step %d, training accuracy %g' % (i, train_accuracy))
            test_accuracy = accuracy.eval(feed_dict={
                x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
            print('step %d, test accuracy %g' % (i, test_accuracy))

        train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})

    print('test accuracy %g' % accuracy.eval(feed_dict={
        x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))
'''
