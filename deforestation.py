import pandas as pd
import numpy as np

import argparse
from itertools import chain
import scipy.misc
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import cpu_count

import torch.nn as nn
from torch.autograd import Variable
import torch
import torch.optim as optim
import torchvision.models as models
from torch.utils.data import Dataset, DataLoader
from torch.utils.data import sampler

from sklearn.model_selection import train_test_split
from sklearn.metrics import fbeta_score
from os.path import join

from tqdm import tqdm

import h5py

import time
# import matplotlib.pyplot as plt
# import matplotlib.image as mpimg

dtype = None
viz = None

DST_SIZE = (224, 224)
IMAGE_KEY = 'imgs'
LABELS_KEY = 'lbls'


class AmazonDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, db_path):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
        """
        super(AmazonDataset, self).__init__()
        self.db = h5py.File(db_path, 'r')
        self.len = self.db[IMAGE_KEY].shape[0]

    def __len__(self):
        return self.len

    def __getitem__(self, idx):

        img = self.db[IMAGE_KEY][idx]
        lbls = self.db[LABELS_KEY][idx]
        img = (img / 255.0).astype(np.float32)
        img = np.transpose(img, (2, 0, 1))
        sample = {'img': img, 'lbl': lbls}
        return sample


'''

class ChunkSampler(sampler.Sampler):
    """Samples elements sequentially from some offset.
    Arguments:
        num_samples: # of desired datapoints
        start: offset where we should start selecting from
    """

    def __init__(self, num_samples, start=0):
        self.num_samples = num_samples
        self.start = start

    def __iter__(self):
        return iter(range(self.start, self.start + self.num_samples))

    def __len__(self):
        return self.num_samples
'''

'''
def reset(m):
    if hasattr(m, 'reset_parameters'):
        m.reset_parameters()
'''


class AmazonModel(nn.Module):

    def __init__(self, pretrained_model, labels_count):
        super(AmazonModel, self).__init__()
        self.pretrained_model = pretrained_model
        # self.pretrained_model_2 = pretrained_model_2
        self.relu = nn.ReLU()
        self.fc1 = nn.Linear(2000, 1000)
        self.fc2 = nn.Linear(1000, labels_count)  # create layer
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x):
        model = self.relu(self.pretrained_model(x))
        # model_2 = self.relu(self.pretrained_model_2(x))
        # out1 = torch.cat((model_1,model_2),1)
        return self.sigmoid(self.fc2(self.relu(model)))


class AmazonHelper:

    def __init__(self, labels_count, gpu=True):
        self.viz = None
        self.win = {}

        self.model = None
        self.dataloaders = {}

        self.threshold = [0.2] * labels_count
        self.num_labels = labels_count

        if gpu:
            self.dtype = torch.cuda.FloatTensor
            print("GPU")
        else:
            self.dtype = torch.FloatTensor
            print("CPU")

    def load_model(self, path):
        print("Model is loading...")
        d = torch.load(path)
        self.model.load_state_dict(d)

    def create_model(self):
        pretrained_model = models.resnet18(pretrained=True)
        self.model = AmazonModel(pretrained_model, self.num_labels)
        self.model.type(self.dtype)
        self.model.apply(self.reset)

    def create_dataloaders(self, db_train, db_val):
        print("Dataset is creating...")
        print('Amazon train dataset is creating...')
        train_dataset = AmazonDataset(db_train)
        print('Amazon validation dataset is creating...')
        val_dataset = AmazonDataset(db_val)
        batch_size = 64
        self.dataloaders['train'] = DataLoader(train_dataset, batch_size=batch_size,
                                               shuffle=True)
        self.dataloaders['val'] = DataLoader(val_dataset, batch_size=batch_size,
                                             shuffle=True)

    def train(self, path_save, num_epochs=60):
        print("Start in training mode.")
        since = time.time()
        # set training parameters
        learning_rate = 1e-3
        #beta_1 = 0.75
        #beta_2 = 0.95
        optimizer = optim.Adam(self.model.parameters(),
                               lr=learning_rate)
        '''
        scheduler = optim.lr_scheduler.StepLR(
            optimizer, step_size=8, gamma=0.1)
        '''
        criterion = nn.BCELoss(size_average=True).type(self.dtype)
        best_model = self.model.state_dict()
        best_fbeta = 0.0
        iter_test_dataset = iter(self.dataloaders['val'])
        i_val = 0
        num_batches = {'train': len(self.dataloaders['train']),
                       'val': len(self.dataloaders['val'])}
        for epoch in range(num_epochs):
            print('Epoch {}/{}'.format(epoch, num_epochs - 1))
            print('-' * 10)
            sum_loss = 0.0
            sum_fbeta = 0.0

            # TRAIN PHASE
            # scheduler.step()
            self.model.train(True)  # Set model to training mode

            for i, data in enumerate(self.dataloaders['train']):

                # get the inputs
                inputs, targets = data['img'], data['lbl']

                # wrap them in Variable
                inputs = Variable(inputs.type(self.dtype))
                targets = Variable(targets.type(self.dtype))
                # zero the parameter gradients
                optimizer.zero_grad()
                # forward
                outputs = self.model(inputs)

                # define loss
                loss = criterion(outputs, targets)
                fbeta = self.fbeta(targets, outputs)
                sum_loss += loss.data[0]
                sum_fbeta += fbeta

                # backward
                loss.backward()

                # optimize
                optimizer.step()

                # visualize training
                if self.viz and (i % 50 == 0):

                    # visualize train metrics
                    X = epoch + float(i) / num_batches['train']
                    self.viz.line(X=np.array([X]), Y=np.array([loss.data[0]]),
                                  win=self.win['loss'][0], name='train-online', update='append')
                    self.viz.line(X=np.array([X]), Y=np.array([fbeta]),
                                  win=self.win['fbeta'][0], name='train-online', update='append')
                    self.viz.image(inputs.cpu().data.numpy()[0], win=self.win['img'], opts=dict(
                        title='Original image', caption='Original image'))
                    self.viz.text(str(targets.cpu().data.numpy()[0]), win=self.win[
                                  'target'], opts=dict(title='Target', caption='Target'))
                    self.viz.text(str(outputs.cpu().data.numpy()[0]), win=self.win[
                                  'pred'], opts=dict(title='Prediction', caption='Prediction'))

                    if i % 200 == 0:
                        # visualize validation metrics
                        self.model.eval()
                        try:
                            test_data = next(iter_test_dataset)
                        except StopIteration:
                            iter_test_dataset = iter(self.dataloaders['val'])
                            test_data = next(iter_test_dataset)
                        i_val = (i_val + 1) % num_batches['val']
                        test_inputs = Variable(
                            test_data['img'].type(self.dtype), volatile=True)
                        test_targets = Variable(
                            test_data['lbl'].type(self.dtype), volatile=True)
                        test_outputs = self.model(test_inputs)
                        test_loss = criterion(test_outputs, test_targets)
                        fbeta = self.fbeta(test_targets, test_outputs)
                        self.viz.line(X=np.array([X]), Y=np.array([test_loss.data[0]]),
                                      win=self.win['loss'][0], name='val-online', update='append')
                        self.viz.line(X=np.array([X]), Y=np.array([fbeta]),
                                      win=self.win['fbeta'][0], name='val-online', update='append')

                        self.model.train(True)
            epoch_loss, epoch_fbeta = sum_loss / \
                num_batches['train'], sum_fbeta / num_batches['train']
            print('Train Loss: {:.4f} Fbeta: {:.4f}'.format(
                epoch_loss, epoch_fbeta))

            if (self.viz):
                self.viz.line(X=np.array([epoch + 1]), Y=np.array([epoch_loss]),
                              win=self.win['loss'][0], name='train-real', update='append')
                self.viz.line(X=np.array([epoch + 1]), Y=np.array([epoch_fbeta]),
                              win=self.win['fbeta'][0], name='train-real', update='append')

            # VALIDATION PHASE

            self.model.eval()
            sum_loss = 0.0
            sum_fbeta = 0.0
            for i, data in enumerate(tqdm(self.dataloaders['val'])):

                # get the inputs
                inputs, targets = data['img'], data['lbl']
                # wrap them in Variable
                inputs = Variable(inputs.type(self.dtype), volatile=True)
                targets = Variable(targets.type(
                    self.dtype), volatile=True)
                # forward
                outputs = self.model(inputs)
                loss = criterion(outputs, targets)
                fbeta = self.fbeta(targets, outputs)
                sum_loss += loss.data[0]
                sum_fbeta += fbeta

            epoch_loss, epoch_fbeta = sum_loss / \
                num_batches['val'], sum_fbeta / num_batches['val']

            if epoch_fbeta > best_fbeta:
                best_fbeta = epoch_fbeta
                best_model = self.model.state_dict()
                self.save_model(path_save)

            print('Val Loss: {:.4f} Fbeta: {:.4f}'.format(
                epoch_loss, epoch_fbeta))

            if (self.viz):
                self.viz.line(X=np.array([epoch + 1]), Y=np.array([epoch_loss]),
                              win=self.win['loss'][0], name='val-real', update='append')
                self.viz.line(X=np.array([epoch + 1]), Y=np.array([epoch_fbeta]),
                              win=self.win['fbeta'][0], name='val-real', update='append')
            print()

        time_elapsed = time.time() - since
        print('Training complete in {:.0f}m {:.0f}s'.format(
            time_elapsed // 60, time_elapsed % 60))
        print('Best val Acc: {:4f}'.format(best_fbeta))
        # load best model weights
        self.model.load_state_dict(best_model)
        pass

    def inference(self, data_root):
        print("Start inference mode.")
        pass

    def save_model(self, path):
        torch.save(self.model.state_dict(), path)

    def fbeta(self, target, output):
        target_numpy = target.cpu().data.numpy()
        output_numpy = output.cpu().data.numpy()
        return fbeta_score(target_numpy[:len(output_numpy)], output_numpy > self.threshold, beta=2, average='samples')

    def reset(self, m):
        if hasattr(m, 'reset_parameters'):
            m.reset_parameters()

    def create_visdom(self, port, host):
        print("Creating plots for train visualisation...")
        import visdom as viz
        self.viz = viz.Visdom(server=host, port=port)
        self.win = {}
        self.win['loss'] = []
        self.win['fbeta'] = []
        self.win['loss'].append(self.viz.line(X=np.array([0]), Y=np.array([[0, 0, 0, 0]]), opts=dict(
            xlabel='Iteration',
            ylabel='Loss',
            title='Loss plot',
            legend=['train-online', 'val-online', 'train-real', 'val-real'],
            showlegend=True
        )))
        self.win['fbeta'].append(self.viz.line(X=np.array([0]), Y=np.array([[0, 0, 0, 0]]),
                                               opts=dict(
            xlabel='Iteration',
            ylabel='Accuracy',
            title='Accuracy plot',
            legend=['train-online', 'val-online', 'train-real', 'val-real'],
            showlegend=True
        )))
        self.win['img'] = viz.image(np.ones((224, 224)),
                                    opts=dict(title='Original image', caption='Original image'))
        self.win['pred'] = viz.text('Prediction', opts=dict(
            title='Prediction', caption='Prediction'))
        self.win['target'] = viz.text(
            'Target', opts=dict(title='Target', caption='Target'))

'''
def validate(model_, loader_valid):
    p_valid = []
    y_valid = []
    for step_val, sample_batched_val in enumerate(loader_valid):
        x_val = Variable(sample_batched_val['image'].type(dtype))
        y_val = Variable(sample_batched_val['labels'].type(dtype))
        output = model_(x_val).data.cpu().numpy()
        p_valid.extend(output)
        y_valid.extend(y_val.data.cpu().numpy())
    return p_valid, y_valid


def test_model(loader_test, path, labels_count=17):
    pretrained_model = models.resnet18(pretrained=True)
    model = Amazon(pretrained_model, labels_count)
    model.load_state_dict(torch.load(path))
    model.type(dtype)
    return validate(model, loader_test)
'''

'''
def get_optimal_threshold(true_label, prediction, iterations=100, labels_count=17):

    best_threshold = [0.2] * labels_count
    for t in range(labels_count):
        best_fbeta = 0
        temp_threshold = [0.2] * labels_count
        for i in range(iterations):
            temp_value = i / float(iterations)
            temp_threshold[t] = temp_value
            temp_fbeta = fbeta(true_label, prediction > temp_threshold)
            if temp_fbeta > best_fbeta:
                best_fbeta = temp_fbeta
                best_threshold[t] = temp_value
    return best_threshold





def map_predictions(predictions, labels_map, thresholds):
    """
    Return the predictions mapped to their labels
    :param predictions: the predictions from the predict() method
    :param labels_map: the map
    :param thresholds: The threshold of each class to be considered as existing or not existing
    :return: the predictions list mapped to their labels
    """
    predictions_labels = []
    for prediction in predictions:
        labels = [labels_map[i]
                  for i, value in enumerate(prediction) if value > thresholds[i]]
        predictions_labels.append(labels)

    return predictions_labels

'''


def main():
    args = parse_args()
    assert(args.train_db is not None)
    assert(args.val_db is not None)
    assert(args.save_model != None)
    helper = AmazonHelper(17)
    if args.load_model:
        helper.load_model(args.load_model)
    else:
        helper.create_model()
    helper.create_dataloaders(
        args.train_db, args.val_db)
    if args.train:
        if args.ui:
            helper.create_visdom(args.port, args.host)
        helper.train(args.save_model)
    else:
        '''
        assert(args.load_model != None)
        checkpoint = torch.load(args.load_model)
        file_size = len(pd.read_csv(args.csv))
        new_size = (224, 224)
        amazon_dataset = AmazonDataset(csv_file=args.csv,
                                       root_dir=args.data, new_size=new_size)
        loader_test = DataLoader(amazon_dataset, batch_size=64,
                                 sampler=ChunkSampler(NUM_TEST, NUM_TRAIN + NUM_VAL))
        y_pred, y_true = test_model(loader_test, args.load_model)
        threshold = [0.2] * 17
        score = fbeta_score(np.array(y_true)[:len(y_pred)], np.array(
            y_pred) > threshold, beta=2, average='samples')
        print("test_score:", score)
        '''
        '''predicted_labels = map_predictions(predictions, y_map, best_threshold)
        tags_list = [None] * len(predicted_labels)
        for i, tags in enumerate(predicted_labels):
            tags_list[i] = ' '.join(map(str, tags))
        final_data = [[filename.split(".")[0], tags]
                      for filename, tags in zip(x_test_filename, tags_list)]
        final_df = pd.DataFrame(final_data, columns=['image_name', 'tags'])
        final_df.head()
        final_df.to_csv('./sub/submission_0.csv', index=False)
        '''


def parse_args():
    parser = argparse.ArgumentParser(description='Augmentation args parser')
    parser.add_argument('--train', action='store_true',
                        help='Launch in train mode')
    parser.add_argument('--train_db', type=str,
                        help='Path to h5 file with train images and labels.')
    parser.add_argument('--val_db', type=str,
                        help='Path to h5 file with validation images and labels.')
    parser.add_argument('--save_model', type=str,
                        help='Path where to save the model')
    parser.add_argument('--load_model', type=str,
                        help='Path where model was saved')
    parser.add_argument('--host', type=str, default='http://localhost',
                        help='Visdom host (localhost by default)')
    parser.add_argument('--port', type=int, default=8097,
                        help='Visdom port (8097 by default)')
    parser.add_argument('--ui', action='store_true', default=False,
                        help='Enable Visdom UI')
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
