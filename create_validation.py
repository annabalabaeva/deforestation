import os
import pandas as pd
import numpy as np

VAL_LEN = 9000


def main():
    args = parse_args()
    df = pd.read_csv(args.csv)
    val_df = df[-VAL_LEN:]
    train_df = df[:-VAL_LEN]
    val_df.index = np.arange(1, len(val_df) + 1)
    val_df.to_csv(args.val_csv, index=False)
    train_df.to_csv(args.train_csv, index=False)
    for i in np.arange(1, VAL_LEN + 1):
        filename = val_df.ix[i, 0] + '.jpg'
        os.rename(os.path.join(args.train_data, filename),
                  os.path.join(args.save_to, filename))


def parse_args():
    """ Parses arguments: creates parser, add command line arguments and calls parser.parse_args.

    Returns:
    Populated namespace (which parser.parse_args returns).
    """
    import argparse
    parser = argparse.ArgumentParser(description='Train UNet')
    parser.add_argument('--train_data', type=str,
                        help='Path to root dir of train.')
    parser.add_argument('--save_to', type=str,
                        help='Path to root dir of new valid dir.')
    parser.add_argument('--csv', type=str,
                        help='Path to train csv.')
    parser.add_argument('--train_csv', type=str,
                        help='Path to new train csv.')
    parser.add_argument('--val_csv', type=str,
                        help='Path to new val csv.')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
