from __future__ import print_function
from __future__ import division
from __future__ import generators

import numpy as np
import tensorflow as tf

import os
import time
import h5py
import skimage
import threading

from cvl.utils import timer


CATEGORIES = ["Black-grass", "Charlock", "Cleavers", "Common Chickweed",
              "Common wheat", "Fat Hen", "Loose Silky-bent", "Maize",
              "Scentless Mayweed", "Shepherds Purse",
              "Small-flowered Cranesbill", "Sugar beet"]

NUM_CATEGORIES = len(CATEGORIES)

IMPORT_MODEL_PATH = "/home/dusyak/plants/resnet18/model_alt/model.ckpt"
EXPORT_MODEL_PATH = "/home/dusyak/plants/best/best_"
TRAIN_DIR = "/home/dusyak/plants/train/"
HDF5_PATH = "/home/dusyak/plants/hdf5/plants.h5"
LOG_DIR = "/home/dusyak/plants/logs/"
SMB_DIR = "/home/dusyak/plants/best/smb/"

MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]

IMG_SIZE = 256
CROPPED_SIZE = 224
VALID_SIZE = 0.3
BATCH_SIZE = 32
EPOCHS = 200
LR_DECAY = 0.95
LR_DECAY_STEP = 2
LEARNING_RATE = 5e-5
NUM_LOADERS = 4
USE_HDF5 = True
USE_QUEUES = True


if USE_QUEUES:
    CURRENT_TRAIN_IMG = 0
    CURRENT_TRAIN_IMG_LOCK = threading.Lock()
    CURRENT_VALID_IMG = 0
    CURRENT_VALID_IMG_LOCK = threading.Lock()


def fix_dataset(tp, tl, vp, vl, batch_size):
    diff = batch_size - (len(tp) % batch_size)

    tp_new = np.array(tp)
    tl_new = np.array(tl)
    vp_new = np.array(vp)
    vl_new = np.array(vl)

    if diff != 0:
        indices = np.random.randint(0, len(vp), diff)
        tp_new = np.append(tp_new, vp_new[indices])
        tl_new = np.append(tl_new, vl_new[indices])
        vp_new = np.delete(vp_new, indices)
        vl_new = np.delete(vl_new, indices)

    return list(tp_new), list(tl_new), list(vp_new), list(vl_new)


def make_paths(train_dir, batch_size):
    train_paths = []
    train_labels = []
    valid_paths = []
    valid_labels = []
    for category_id, category in enumerate(CATEGORIES):
        for f in os.listdir(os.path.join(train_dir, category)):
            if np.random.uniform(0, 1) <= VALID_SIZE:
                valid_paths.append(os.path.join(train_dir, category, f))
                valid_labels.append(category_id)
            else:
                train_paths.append(os.path.join(train_dir, category, f))
                train_labels.append(category_id)

    return fix_dataset(
        train_paths, train_labels, valid_paths, valid_labels, batch_size
    )


def make_paths_hdf5(labels_db, batch_size):
    train_img_idxs = []
    train_labels = []
    valid_img_idxs = []
    valid_labels = []

    for idx, lb in enumerate(labels_db):
        if np.random.uniform(0, 1, 1)[0] <= VALID_SIZE:
            valid_labels.append(lb)
            valid_img_idxs.append(idx)
        else:
            train_labels.append(lb)
            train_img_idxs.append(idx)

    return fix_dataset(
        train_img_idxs, train_labels, valid_img_idxs, valid_labels, batch_size
    )


def shuffle_unison(*args):
    shuffled_indices = np.random.permutation(len(args[0]))
    return [a[shuffled_indices] for a in args]


if USE_HDF5:
    file = h5py.File(HDF5_PATH, "r")

    IMAGES_DB = file["/images"]
    LABELS_DB = file["/labels"]
    TRAIN_INDICES, TRAIN_LABELS, VALID_INDICES, VALID_LABELS = make_paths_hdf5(
        LABELS_DB, BATCH_SIZE
    )
    TRAIN_INDICES, TRAIN_LABELS = shuffle_unison(
        np.array(TRAIN_INDICES), np.array(TRAIN_LABELS)
    )


def valid_generator():
    for img, lb in zip(VALID_INDICES, VALID_LABELS):
        img2 = skimage.img_as_float(IMAGES_DB[img])
        yield img2, lb


def train_generator():
    for img, lb in zip(TRAIN_INDICES, TRAIN_LABELS):
        img2 = skimage.img_as_float(IMAGES_DB[img])
        yield img2, lb


def random_crop_np(img, target_h, target_w):
    range_h = img.shape[0] - target_h
    range_w = img.shape[1] - target_w

    start_h = np.random.randint(0, range_h, 1)[0]
    start_w = np.random.randint(0, range_w, 1)[0]

    cropped_img = img[start_h:start_h + target_h,
                      start_w:start_w + target_w,
                      :]
    return cropped_img


def central_crop_np(img, target_h, target_w):
    assert img.shape[0] % 2 == 0
    assert img.shape[1] % 2 == 0

    h_start = (img.shape[0] - target_h) // 2
    w_start = (img.shape[1] - target_w) // 2
    return img[h_start:h_start + target_h, w_start:w_start + target_w, :]


def _random_crop(img, target_h, target_w):
    range_h = img.shape[0].value - target_h
    range_w = img.shape[1].value - target_w

    start_h = np.random.randint(0, range_h, 1)[0]
    start_w = np.random.randint(0, range_w, 1)[0]

    cropped_img = img[start_h:start_h + target_h,
                      start_w:start_w + target_w,
                      :]
    return cropped_img


def _augment_train(filename, label):
    img = tf.image.decode_png(tf.read_file(filename), channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    img = tf.image.resize_images(img, [IMG_SIZE, IMG_SIZE])
    img = _random_crop(img, CROPPED_SIZE, CROPPED_SIZE)
    img = tf.image.random_flip_left_right(img)
    img = tf.image.random_flip_up_down(img)
    img = (img - MEAN) / STD

    return img, label


def _augment_valid(filename, label):
    img = tf.image.decode_png(tf.read_file(filename), channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    img = tf.image.resize_images(img, [IMG_SIZE, IMG_SIZE])
    img = tf.image.central_crop(img, CROPPED_SIZE / IMG_SIZE)
    img = (img - MEAN) / STD

    return img, label


def _augment_train_hdf5(image, label):
    img = _random_crop(image, CROPPED_SIZE, CROPPED_SIZE)
    img = tf.image.random_flip_left_right(img)
    img = tf.image.random_flip_up_down(img)
    img = (img - MEAN) / STD

    return img, label


def _augment_valid_hdf5(image, label):
    img = tf.image.central_crop(image, CROPPED_SIZE / IMG_SIZE)
    img = (img - MEAN) / STD

    return img, label


def enqueue_valid_worker(sess, enqueue_op, image_ph, label_ph, coord):
    global CURRENT_VALID_IMG
    global VALID_INDICES
    global VALID_LABELS

    with coord.stop_on_exception():
        while not coord.should_stop():
            with CURRENT_VALID_IMG_LOCK:
                idx = VALID_INDICES[CURRENT_VALID_IMG]
                label = VALID_LABELS[CURRENT_VALID_IMG]

                if CURRENT_VALID_IMG != len(VALID_INDICES) - 1:
                    CURRENT_VALID_IMG += 1
                else:
                    CURRENT_VALID_IMG = 0

                image = skimage.img_as_float(IMAGES_DB[idx])

            image = central_crop_np(image, CROPPED_SIZE, CROPPED_SIZE)
            image = (image - MEAN) / STD
            sess.run(enqueue_op, {image_ph: image, label_ph: label})


def enqueue_train_worker(sess, enqueue_op, image_ph, label_ph, coord):
    global CURRENT_TRAIN_IMG
    global TRAIN_INDICES
    global TRAIN_LABELS

    with coord.stop_on_exception():
        while not coord.should_stop():
            with CURRENT_TRAIN_IMG_LOCK:
                idx = TRAIN_INDICES[CURRENT_TRAIN_IMG]
                label = TRAIN_LABELS[CURRENT_TRAIN_IMG]

                if CURRENT_TRAIN_IMG != len(TRAIN_INDICES) - 1:
                    CURRENT_TRAIN_IMG += 1
                else:
                    CURRENT_TRAIN_IMG = 0
                    TRAIN_INDICES, TRAIN_LABELS = shuffle_unison(
                        TRAIN_INDICES, TRAIN_LABELS
                    )
                image = skimage.img_as_float(IMAGES_DB[idx])

            image = random_crop_np(image, CROPPED_SIZE, CROPPED_SIZE)

            #  random flips
            flips = np.random.randint(0, 2, 2)
            if flips[0] == 1:
                image = np.flip(image, 0)
            if flips[1] == 1:
                image = np.flip(image, 1)

            image = (image - MEAN) / STD
            sess.run(enqueue_op, {image_ph: image, label_ph: label})


def main():
    if USE_HDF5 and USE_QUEUES:
        train_img_ph = tf.placeholder(
            tf.float32,
            (CROPPED_SIZE, CROPPED_SIZE, 3),
            name="new_train_input"
        )
        train_lbl_ph = tf.placeholder(tf.int32, (), name="new_train_input")

        train_queue = tf.FIFOQueue(
            capacity=BATCH_SIZE * 5,
            dtypes=[tf.float32, tf.int32],
            shapes=[(CROPPED_SIZE, CROPPED_SIZE, 3), ()]
        )

        train_enqueue_op = train_queue.enqueue(
            [train_img_ph, train_lbl_ph])
        train_dequeue_op = train_queue.dequeue_many(BATCH_SIZE)

        valid_img_ph = tf.placeholder(
            tf.float32,
            (CROPPED_SIZE, CROPPED_SIZE, 3),
            name="new_valid_input"
        )
        valid_lbl_ph = tf.placeholder(tf.int32, (), name="new_valid_input")

        valid_queue = tf.FIFOQueue(
            capacity=BATCH_SIZE * 3,
            dtypes=[tf.float32, tf.int32],
            shapes=[(CROPPED_SIZE, CROPPED_SIZE, 3), ()]
        )

        valid_enqueue_op = valid_queue.enqueue(
            [valid_img_ph, valid_lbl_ph])
        valid_dequeue_op = valid_queue.dequeue_many(BATCH_SIZE)

        valid_ph = tf.placeholder_with_default(False, (), "validation_on")

        next_batch = tf.cond(tf.equal(valid_ph, tf.convert_to_tensor(True)),
                             lambda: valid_dequeue_op,
                             lambda: train_dequeue_op)
        numpy_batch = np.array(next_batch)
        next_images = numpy_batch[0]
        next_labels = numpy_batch[1]

    elif USE_HDF5 and not USE_QUEUES:
        train_paths, train_labels, valid_paths, valid_labels = make_paths_hdf5(
            LABELS_DB, BATCH_SIZE
        )

        train_ds = tf.data.Dataset.from_generator(
            generator=train_generator,
            output_types=(tf.float32, tf.int32),
            output_shapes=(tf.TensorShape([IMG_SIZE, IMG_SIZE, 3]),
                           tf.TensorShape([]))
        )
        valid_ds = tf.data.Dataset.from_generator(
            generator=valid_generator,
            output_types=(tf.float32, tf.int32),
            output_shapes=(tf.TensorShape([IMG_SIZE, IMG_SIZE, 3]),
                           tf.TensorShape([]))
        )

        train_ds = train_ds.map(
            _augment_train_hdf5, num_parallel_calls=NUM_LOADERS
        ).repeat()
        valid_ds = valid_ds.map(
            _augment_valid_hdf5, num_parallel_calls=NUM_LOADERS
        ).repeat()

    else:
        train_paths, train_labels, valid_paths, valid_labels = make_paths(
            TRAIN_DIR, BATCH_SIZE
        )
        train_ds = tf.data.Dataset.from_tensor_slices(
            (train_paths, train_labels)
        )
        valid_ds = tf.data.Dataset.from_tensor_slices(
            (valid_paths, valid_labels)
        )

        train_ds = train_ds.map(_augment_train)
        valid_ds = valid_ds.map(_augment_valid)

    if not USE_QUEUES:
        train_ds = train_ds.shuffle(BATCH_SIZE * 10)
        batched_train_ds = train_ds.batch(BATCH_SIZE)
        batched_valid_ds = valid_ds.batch(BATCH_SIZE)
        batched_train_ds = batched_train_ds.prefetch(BATCH_SIZE * 10)
        batched_valid_ds = batched_valid_ds.prefetch(BATCH_SIZE * 5)

        handle = tf.placeholder(tf.string, shape=[])
        data_iter = tf.data.Iterator.from_string_handle(
            handle,
            batched_train_ds.output_types,
            batched_train_ds.output_shapes
        )

        train_init = batched_train_ds.make_initializable_iterator()
        valid_init = batched_valid_ds.make_initializable_iterator()

        next_images, next_labels = data_iter.get_next()

    if USE_QUEUES:
        train_epoch_iters = len(TRAIN_INDICES) // BATCH_SIZE
        valid_epoch_iters = len(VALID_INDICES) // BATCH_SIZE
    else:
        train_epoch_iters = len(train_paths) // BATCH_SIZE
        valid_epoch_iters = len(valid_paths) // BATCH_SIZE

    #  creating exponential lr
    global_step = tf.Variable(0, trainable=False)
    decayed_lr = tf.train.exponential_decay(
        learning_rate=LEARNING_RATE,
        global_step=global_step,
        decay_steps=LR_DECAY_STEP * train_epoch_iters,
        decay_rate=LR_DECAY,
        staircase=True
    )

    #  importing resnet-18
    next_im_ph = tf.placeholder_with_default(
        next_images, (None, CROPPED_SIZE, CROPPED_SIZE, 3)
    )
    next_lb_ph = tf.placeholder_with_default(next_labels, None)

    old_saver = tf.train.import_meta_graph(
        IMPORT_MODEL_PATH + ".meta",
        input_map={"Placeholder:0": next_im_ph,
                   "Placeholder_1:0": next_lb_ph,
                   "lr:0": decayed_lr})

    graph = tf.get_default_graph()

    sess = tf.Session(
        config=tf.ConfigProto(gpu_options={"allow_growth": True}))
    old_saver.restore(sess, IMPORT_MODEL_PATH)

    old_summaries = tf.get_collection_ref(tf.GraphKeys.SUMMARIES)
    old_summaries.pop()
    old_summaries.pop()

    #  getting nodes from imported graph
    train_ph = graph.get_tensor_by_name("is_train:0")
    old_logits = graph.get_tensor_by_name("tower/logits/fc/BiasAdd:0")

    #  adding new outputs
    with tf.name_scope("tower/logits_plants"):
        logits_plants = tf.layers.dense(tf.nn.relu(old_logits),
                                        NUM_CATEGORIES,
                                        name="logits_plants")

    #  adding loss
    with tf.name_scope("loss_plants"):
        one_hot_labels = tf.one_hot(next_labels, NUM_CATEGORIES)
        plants_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
            logits=logits_plants, labels=one_hot_labels))

    #  adding accuracy
    with tf.name_scope("plants_accuracy"):
        softmax = tf.nn.softmax(logits_plants, name="plants_softmax")
        predict = tf.cast(tf.argmax(softmax, axis=1), tf.int32)
        plants_acc = tf.reduce_mean(
            tf.cast(tf.equal(predict, next_labels), tf.float32)
        )

    #  adding optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate=decayed_lr)
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    #  adding train_op
    with tf.control_dependencies(update_ops):
        train = optimizer.minimize(plants_loss, global_step=global_step)

    #  Placeholder for metrics per epoch
    ep_loss = tf.placeholder(tf.float32, shape=(), name="epoch_loss")
    ep_acc = tf.placeholder(tf.float32, shape=(), name="epoch_accuracy")
    tf.summary.scalar("ep_acc", ep_acc)
    tf.summary.scalar("ep_loss", ep_loss)

    #  Savers
    sv_train = tf.summary.FileWriter(os.path.join(LOG_DIR, "train"),
                                     graph=graph,
                                     flush_secs=40)
    sv_test = tf.summary.FileWriter(os.path.join(LOG_DIR, "test"),
                                    flush_secs=40)
    merged = tf.summary.merge_all()

    best_acc = 0

    new_saver = tf.train.Saver()

    sess.run(tf.global_variables_initializer())

    if USE_QUEUES:
        coord = tf.train.Coordinator()

        #  starting train loaders
        for _ in range(NUM_LOADERS):
            t = threading.Thread(
                target=enqueue_train_worker,
                args=(sess,
                      train_enqueue_op,
                      train_img_ph,
                      train_lbl_ph,
                      coord)
            )
            t.start()
            coord.register_thread(t)

        #  starting valid loaders
        for _ in range(NUM_LOADERS):
            t = threading.Thread(
                target=enqueue_valid_worker,
                args=(sess,
                      valid_enqueue_op,
                      valid_img_ph,
                      valid_lbl_ph,
                      coord)
            )
            t.start()
            coord.register_thread(t)

        #  try to learn
        try:
            for ep in np.arange(EPOCHS):
                start_time = time.time()
                print("Epoch {}/{}".format(ep + 1, EPOCHS))

                train_loss_ep_hist = []
                train_acc_ep_hist = []

                #  train
                for _ in np.arange(train_epoch_iters):
                    _, batch_loss, batch_acc = sess.run(
                        [train, plants_loss, plants_acc],
                        feed_dict={train_ph: True, valid_ph: False}
                    )
                    train_loss_ep_hist.append(batch_loss)
                    train_acc_ep_hist.append(batch_acc)

                ep_train_loss = np.mean(train_loss_ep_hist)
                print("Train loss:", ep_train_loss)

                ep_train_acc = np.mean(train_acc_ep_hist)
                print("Train acc:", ep_train_acc)

                summary = sess.run(
                    merged,
                    feed_dict={ep_loss: ep_train_loss,
                               ep_acc: ep_train_acc})
                sv_train.add_summary(summary, ep)

                valid_loss_ep_hist = []
                valid_acc_ep_hist = []

                #  valididation
                for _ in np.arange(valid_epoch_iters):
                    batch_loss, batch_acc = sess.run(
                        [plants_loss, plants_acc],
                        feed_dict={train_ph: False, valid_ph: True}
                    )
                    valid_loss_ep_hist.append(batch_loss)
                    valid_acc_ep_hist.append(batch_acc)

                ep_valid_loss = np.mean(valid_loss_ep_hist)
                print("valid loss:", ep_valid_loss)

                ep_valid_acc = np.mean(valid_acc_ep_hist)
                print("valid acc:", ep_valid_acc)

                summary = sess.run(
                    merged,
                    feed_dict={ep_loss: ep_valid_loss,
                               ep_acc: ep_valid_acc})
                sv_test.add_summary(summary, ep)

                print("Time per epoch:", time.time() - start_time)

                if ep_valid_acc > best_acc:
                    best_acc = ep_valid_acc
                    new_saver.save(sess, EXPORT_MODEL_PATH)

                print("Best acc on valid:", best_acc)
                print("=" * 40)
        except Exception as e:
            coord.request_stop(e)
        finally:
            coord.request_stop()
            sess.run([train_queue.close(cancel_pending_enqueues=True),
                      valid_queue.close(cancel_pending_enqueues=True)])
            coord.join()

    else:
        training_handle = sess.run(train_init.string_handle())
        validation_handle = sess.run(valid_init.string_handle())

        for ep in np.arange(EPOCHS):
            start_time = time.time()
            print("Epoch {}/{}".format(ep + 1, EPOCHS))

            sess.run(train_init.initializer)
            train_loss_ep_hist = []
            train_acc_ep_hist = []

            #  training
            for _ in np.arange(train_epoch_iters):
                _, batch_loss, batch_acc = sess.run(
                    [train, plants_loss, plants_acc],
                    feed_dict={train_ph: True, handle: training_handle}
                )

                train_loss_ep_hist.append(batch_loss)
                train_acc_ep_hist.append(batch_acc)

            ep_train_loss = np.mean(train_loss_ep_hist)
            print("Train loss:", ep_train_loss)

            ep_train_acc = np.mean(train_acc_ep_hist)
            print("Train acc:", ep_train_acc)

            _, _, summary = sess.run(
                [ep_loss, ep_acc, merged],
                feed_dict={ep_loss: ep_train_loss, ep_acc: ep_train_acc})
            sv_train.add_summary(summary, ep)

            #  evaliduating
            valid_loss_ep_hist = []
            valid_acc_ep_hist = []
            sess.run(valid_init.initializer)

            for _ in np.arange(valid_epoch_iters):
                valid_loss_ev, valid_acc_ev = sess.run(
                    [plants_loss, plants_acc],
                    feed_dict={train_ph: False, handle: validation_handle}
                )

                valid_loss_ep_hist.append(valid_loss_ev)
                valid_acc_ep_hist.append(valid_acc_ev)

            ep_valid_loss = np.mean(valid_loss_ep_hist)
            print("valid loss:", ep_valid_loss)

            ep_valid_acc = np.mean(valid_acc_ep_hist)
            print("valid acc:", ep_valid_acc)

            summary = sess.run(
                merged,
                feed_dict={ep_loss: ep_valid_loss, ep_acc: ep_valid_acc})
            sv_test.add_summary(summary, ep)

            print("Time per epoch:", time.time() - start_time)

            if ep_valid_acc > best_acc:
                best_acc = ep_valid_acc
                new_saver.save(sess, EXPORT_MODEL_PATH)

            print("Best acc on valid:", best_acc)
            print("=" * 40)
    sess.close()


if __name__ == "__main__":
    main()

if USE_HDF5:
    file.close()


# def get_generator(arg1, arg2):
#     def generator():
#         yield arg1, arg2
#     return generator